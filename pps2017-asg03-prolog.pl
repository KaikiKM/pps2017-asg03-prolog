%%%%%%%%%%%%%%%%%%%%% LAB PROLOG 1 %%%%%%%%%%%%%%%%%%%%%


% search(?Elem, ?List)
% example: search(a, [a, b, c]).
% example: search(X, [a, b, c]).
% example: search(a, X).
search(X, [X|_]).
search(X, [_|Xs]):- search(X, Xs).

% search2(?Elem, ?List)
% looks for two consecutive occurrences of Elem
% example: search2(a, [d, a, a, b, c]).
% example: search2(X, [d, a, a, b, c, c]).
% example: search2(a, X).
search2(X, [X,X|_]).
search2(X, [_|Xs]):- search2(X, Xs).

% search_two(?Elem, ?List)
% looks for two occurrences of Elem with an element in between!
% example: search_two(a, [d, a, b, a, b, c, c]).
% example: search_two(X, [d, a, b, a, b, c, c]).
% example: search_two(a, X).
search_two(X, [X,_,X|_]).
search_two(X, [_|Xs]):- search_two(X, Xs).

% search_anytwo(?Elem, ?List)
% looks for any Elem that occurs two times
% example: search_anytwo(c, [d, a, b, a, b, c, c]).
% example: search_anytwo(a, [d, a, b, a, b, c, c]).
% example: search_anytwo(X, [d, a, b, a, b, c, c]).
% example: search_anytwo(a, X).
search_anytwo(X, [X|Xs]):- search(X, Xs).
search_anytwo(X, [_|Xs]):- search_anytwo(X, Xs).

% size(@List, -Size)
% Size will contain the number of elements in List
% example: size([a, b, c], 3).
% example: size([a, b, c], X).
size([], 0).
size([_|T], M):- size(T, N), M is N+1.

% my_size(?List, ?Size)
% Size will contain the number of elements in List
% example: my_size([a, b, c], s(s(s(zero)))).
% example: my_size([a, b, c], X).
% example: my_size(X, s(s(s(zero)))).
my_size([], zero).
my_size([_|T], s(M)):- my_size(T, M).

% sum(@List, -Sum)
% example: sum([1,2,3],6).
% example: sum([1,2,3,3],X).
sum([], 0).
sum([H|T], S):- sum(T, Ss), S is Ss + H.

% average(@List, -Average)
% it uses average(List,Count,Sum,Average)
% example: average([3,4,3],A).
average(List, A):- average(List, 0, 0, A).
average([], S, C, A):- A is S/C.
average([H|T], S, C, A):- S2 is S + H, C2 is C + 1, average(T, S2, C2, A).

% max(@List, -Max)
% Max is the biggest element in List
% Suppose the list has at least one element
% example: max([3,4,3], 4).
% example: max([3,4,3,7], X).
max([H|T], Max):- max(T, Max, H).
max([H|T], Max, TMax):- H >= TMax, max(T, Max, H).
max([H|T], Max, TMax):- H < TMax, max(T, Max, TMax).
max([], Max, TMax):- Max is TMax.

% same(?List1, ?List2)
% are the two lists the same?
% example: same([1,7,4], [1,7,4]).
% example: same(X, [1,7,4]).
% example: same([1,7,4], X).
same([], []).
same([X|Xs], [X|Ys]):- same(Xs, Ys).

% all_bigger(@List1, @List2)
% all elements in List1 are bigger than those in List2, 1 by 1
% example: all_bigger([10,20,30,40],[9,19,29,39]).
all_bigger([], []).
all_bigger([H1|T1], [H2|T2]):- H1 > H2, all_bigger(T1, T2).

% sublist(@List1, @List2)
% List1 should be a subset of List2
% example: sublist([1,2],[5,3,2,1]).
sublist([], [_|_]). %used to avoid sublist([], a) -> true | alternative -> sublist([], List).
sublist([], []). %special case, also covered by sublist([], List).
sublist([H|T], List) :- search(H, List), sublist(T, List).

% seq(@N, -List)
% example: seq(5,[0,0,0,0,0]).
% example: seq(5,X).
seq(0, []).
seq(N, [0|T]):- N > 0, N2 is N-1, seq(N2, T).

% seqR(?N,?List)
% example: seqR(4,[4,3,2,1,0]).
% example: seqR(4,X).
% example: seqR(X,[4,3,2,1,0]).
seqR(0, [0]).
seqR(N, [N|T]):- N>0, N2 is N-1, seqR(N2, T).

% app(?List, ?Elem, ?ResultList)
% example: app([1, 2], [7, 8], [1, 2, 7, 8]).
% example: app([1, 2], 7, [1, 2, 7]).
app([], [], []):- !.
app([], [H|T], [H|T]):- !.
app([H|T], [], [H|T]):- !.
app([H1|T1], [H2|T2], [H1|R2]):- app(T1, [H2|T2], R2), !.
app([], E, [E]).
app([H|T], E, [H|T2]) :- app(T, E, T2).

% seqR2(@N, -List)
% example: seqR2(4,[0,1,2,3,4]).
% example: seqR(4,X).
seqR2(0, [0]).
seqR2(N, List):- N>0, N2 is N-1, seqR2(N2, L), app(L, N, List).

% inv(@List, -List)
% example: inv([1,2,3],[3,2,1]).
% example: inv([1,2,3],X).
inv([], []).
inv([H|T], R):- inv(T, R2), app(R2, H, R).

% double(?List, ?List)
% example: double([1,2,3],[1,2,3,1,2,3]).
% example: double([1,2,3],X).
% example: double(X,[1,2,3,1,2,3]).
double(L, R):- app(L, L, R).

% times(@List, @N, -List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
% example: times([1,2,3],3,X).
% example: times(X,3,[1,2,3,1,2,3,1,2,3]).
% example: times([1,2,3],X,[1,2,3,1,2,3,1,2,3]).
times(L, 0, []).
times(L, N, R):-  N > 0, N2 is N - 1, times(L, N2, R2), app(L, R2, R).

% proj(?List, ?List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).
% example: proj([[1,2],[3,4],[5,6]],X).
% example: proj(X,[1,3,5]).
proj([], []).
proj([[H|Ti]|To], [H|Tr]):- proj(To, Tr).


%%%%%%%%%%%%%%%%%%%%% LAB PROLOG 2 %%%%%%%%%%%%%%%%%%%%%


% dropAny(?Elem, ?List, ?OutList)
% example: dropAny(10,[10,20,10,30,10],L).
dropAny(X, [X|T], T).
dropAny(X, [H|Xs], [H|L]):- dropAny(X, Xs, L).

% dropFirst(?Elem, ?List, ?OutList)
% example: dropFirst(10,[10,20,10,30,10],L).
dropFirst(X, [X|T], T):- !.
dropFirst(X, [H|Xs], [H|L]):- dropFirst(X, Xs, L).

% dropLast(?Elem, ?List, ?OutList)
% example: dropLast(10,[10,20,10,30,10],L).
dropLast(X, [H|Xs], [H|L]):-dropLast(X, Xs, L), !.
dropLast(X, [X|T], T).

% dropAll(?Elem, ?List, ?OutList)
% example: dropAll(10,[10,20,10,30,10],L).
% example: dropAll(e(1,_),[e(1,2),e(1,3),e(2,3)],X).
dropAll(X, [], []).
dropAll(X, [H|T], R):- copy_term(X, X2), H = X2, dropAll(X, T, R), !.
dropAll(X, [H|Xs], [H|L]):- dropAll(X, Xs, L).

% fromList(+List,-Graph)
fromList([_], []).
fromList([H1,H2|T], [e(H1,H2)|L]):- fromList([H2|T], L).

% fromCircList(+List,-Graph)
% example: fromCircList([10,20,30],[e(10,20),e(20,30),e(30,10)]).
% example: fromCircList([10,20],[e(10,20),e(20,10)]).
% example: fromCircList([10],[e(10,10)]).
fromCircList([E], [e(E, H)], H).
fromCircList([H1, H2|T], [e(H1, H2)|Tc], H):-fromCircList([H2|T], Tc, H).
fromCircList([H|T], G):- fromCircList([H|T], G, H).

% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node
% use dropAll defined in 1.1
% example: dropNode([e(1,2),e(1,3),e(2,3)],1,[e(2,3)]).
dropNode(G, N, O):- dropAll(e(N,_), G, G2), dropAll(e(_,N), G2, O).

% reaching(+Graph, +Node, -List)
% example: reaching([e(1,2),e(1,3),e(2,3)],1,L).
% example: reaching([e(1,2),e(1,2),e(2,3)],1,L).
reaching(G, N, L):- findall(X, member(e(N, X), G), L).

% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1
anypath(G, N1, N2, [e(N1,N2)]):- member(e(N1,N2), G).
anypath(G, N1, N2, [e(N1,N3)|T]):- member(e(N1,N3), G), anypath(G, N3, N2, T).

% allreaching(+Graph, +Node, -List)
allreaching(G, N, L):- findall(X, anypath(G, N, X, _), L).


%%%%%%%%%%%%%%%%%%%%% TIC TAC TOES %%%%%%%%%%%%%%%%%%%%%

% nextPlayer(@Player)
player(x).
player(o).

% nextPlayer(+CurrentPlayer, -NextPlayer)
nextPlayer(CP, NP):- player(CP), player(NP), NP \= CP.

% inRange(?Value, @TableDimension)
inRange(C, D):- M is D-1, seqR(M, VL), member(C, VL).

% inLine(@Table, @Pattern, @Number)
inLine(T, P, N):- findall(_, member(P, T), ML), size(ML, N).

% result(@Table, @TableDimension, -Result)
% example: result([cell(0,0, x), cell(0,1,x), cell(0,2,x)], 3, won(x)).
% example: result([cell(0,0, x), cell(0,1,x), cell(0,2,x), cell(0,3,x)], 4, won(x)).
result(T, D, won(P)):- player(P), inRange(Row, D), inLine(T, cell(Row,_,P), D), !.
% example: result([cell(0,1,x), cell(1,1,x), cell(2,1,x)], 3, won(x)).
% example: result([cell(0,1,x), cell(1,1,x), cell(2,1,x), cell(3,1,x)], 4, won(x)).
result(T, D, won(P)):- player(P), inRange(Col, D), inLine(T, cell(_,Col,P), D), !.
% example: result([cell(0,0,x), cell(1,1,x), cell(2,2,x)], 3, won(x)).
% example: result([cell(0,0,x), cell(1,1,x), cell(2,2,x), cell(3,3,x)], 4, won(x)).
result(T, D, won(P)):- player(P), inLine(T, cell(X, X, P), D), !.
% example: result([cell(0,2,x), cell(1,1,x), cell(2,0,x)], 3, won(x)).
% example: result([cell(0,3,x), cell(1,2,x), cell(2,1,x), cell(3,0,x)], 4, won(x)).
result(T, D, won(P)):- player(P), findall(_, (inRange(X, D), Y is D - 1 - X, member(cell(X,Y,P), T)), ML), size(ML, D), !.
% example: result([cell(0,0,o), cell(0,1,x), cell(0,2,x), cell(1,0,x), cell(1,1,x), cell(1,2,o), cell(2,0,o), cell(2,1,o), cell(2,2,x)], 3, even).
% example: result([cell(0,0,o), cell(0,1,x), cell(1,0,x), cell(1,1,x)], 2, even).
result(T, D, even):- S is D * D, size(T, S), !.
% example: result([cell(0,0,o), cell(0,1,x), cell(1,0,x), cell(1,1,x)], 4, nothing).
result(T, D, nothing).

% next(@Table, @TableDimension, @Player, -Result, -NewTable)
next(T, D, P, R, NT):- player(P), inRange(Row, D), inRange(Col, D), \+member(cell(Row,Col,_), T), NT = [cell(Row,Col,P)|T], result(NT, D, R).

% game(@Table, @TableDimension, @Player, -Result, -TableList)
% example: customGame([cell(0,1,x), cell(1,2,o), cell(2,1,x)], 3, o, R, TL).
% example: customGame([cell(0,0,x), cell(0,1,o), cell(0,2,x), cell(1,0,x), cell(1,1,x), cell(1,2,o), cell(2,2,o)], 3, o, R, TL).
% example: customGame([cell(0,0,x), cell(0,1,o), cell(0,2,x), cell(1,0,x), cell(1,1,x), cell(1,2,o), cell(2,2,o)], 3, x, R, TL).
% example: customGame([cell(0,0,x), cell(0,1,o)], 2, x, R, TL).
% example: customGame([cell(0,0,x), cell(0,1,o)], 2, o, R, TL).
customGame(T, D, P, R, TL):- result(T, D, CR), customGame(T, D, P, CR, R, TL).
customGame(T, D, P, nothing, R, [T|TL]):- !, next(T, D, P, NS, NT), nextPlayer(P, NP), customGame(NT, D, NP, NS, R, TL).
customGame(T, D, _, CR, CR, [T]).

% game(@Table, @Player, -Result, -TableList)
% example: game([cell(0,1,x), cell(1,2,o), cell(2,1,x)], o, R, TL).
% example: game([cell(0,0,x), cell(0,1,o), cell(0,2,x), cell(1,0,x), cell(1,1,x), cell(1,2,o), cell(2,2,o)], o, R, TL).
% example: game([cell(0,0,x), cell(0,1,o), cell(0,2,x), cell(1,0,x), cell(1,1,x), cell(1,2,o), cell(2,2,o)], x, R, TL).
game(T, P, R, TL):- customGame(T, 3, P, CR, R, TL).